const eventBrite = new EventBrite();
const ui = new Interfaz();

// Listener al buscador

document.getElementById('buscarBtn').addEventListener('click', (e) => {
    e.preventDefault();

    // Leer el texto del input buscar
    const textoBuscador = document.getElementById('evento').value;

    // Leer select
    const categorias = document.getElementById('listado-categorias');
    const categoriaSeleccionada = categorias.options[categorias.selectedIndex].value;

    // Revisar que haya algo escrito en el buscador
    if (textoBuscador === '') {
        // Mostrar mensaje para que imprima algo
        ui.mostrarMensaje('Escribe algo en el buscador', 'alert alert-danger mt-4');
    }else {
        // Cuando si hay una busqueda
        eventBrite.obtenerEventos(textoBuscador, categoriaSeleccionada)
            .then(data => {
                if (data.eventos.events.length <= 0) {
                    // No hay eventos enviar una alerta
                    ui.mostrarMensaje('No pudimos encontrar ningun resultado para eventos', 'alert alert-danger mt-4');
                } else {
                    // Si hay eventos, mostrar resultado
                    ui.limpiarResultados();
                    ui.mostrarEventos(data);
                }
            })
    }
})